package com.example.springbootpaymentapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootPaymentApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootPaymentApiApplication.class, args);
	}

}
